
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

import clickOutsideHandler from '@ckeditor/ckeditor5-ui/src/bindings/clickoutsidehandler';
import ContextualBalloon from '@ckeditor/ckeditor5-ui/src/panel/balloon/contextualballoon';
import ClickObserver from '@ckeditor/ckeditor5-engine/src/view/observer/clickobserver';

import fontColorIcon from '@ckeditor/ckeditor5-font/theme/icons/font-color.svg';

import FontColorCommand from './fontcolorcommand';

import ColorTemplateContainer from '../ui/colortemplatecontainer';
import ColorTemplate from '../ui/colortemplate';

import ColorUIView from '../ui/coloruiview';
import { renderDowncastElement, renderUpcastAttribute, getColor, isColor, hexToRGB } from '../utils';

export default class CustomFontColors extends Plugin
{
	static get requires()
	{
		return [ ContextualBalloon ];
	}

	static get pluginName()
	{
		return 'CustomFontColors';
    }
    
	static get authorName()
	{
		return 'Kylian Dekker';
    }
    
    constructor( editor )
    {
        super( editor );

		editor.conversion.for('upcast').elementToAttribute( {
			view:
			{
				name: 'span',
				styles:
				{
					'color': /[\s\S]+/
				}
			},
			model:
			{
				key: 'fontColor',
				value: renderUpcastAttribute('color')
			}
		} );

		editor.conversion.for('downcast').attributeToElement( {
			model: 'fontColor',
			view: renderDowncastElement('color')
		} );

		editor.commands.add('fontColor', new FontColorCommand( editor ) );

		editor.model.schema.extend('$text', { allowAttributes: 'fontColor' });

		editor.model.schema.setAttributeProperties('fontColor', { isFormatting: true });
    }

	init()
	{
        const editor = this.editor;

		this.useHex = this.editor.config.get('customFontColors.hex');
		this.useRGB = this.editor.config.get('customFontColors.RGB');
		this.useColorTemplates = this.editor.config.get('customFontColors.colorTemplates');
		this.useOpacitySlider = this.editor.config.get('customFontColors.opacitySlider');
		this.useRemoveButton = this.editor.config.get('customFontColors.removeButton');
		this.infoMessages = this.editor.config.get('customFontColors.infoMessages');

		if (this.infoMessages)
		{
			console.info(
				'Loaded ' + 
				'CustomFontColors' + 
				'by ' +
				'Kylian Dekker' +
				' with config: useHex=\'' + 
				this.useHex +
				'\', useRGB: \'' + 
				this.useRGB + 
				'\', useColorTemplates: \'' +
				this.useColorTemplates + 
				'\', useRemoveButton: \'' +
				this.useRemoveButton + 
				'\', useOpacitySlider: \'' +
				this.useOpacitySlider + 
				'\'.');
		}

		editor.editing.view.addObserver( ClickObserver );

		this.formView = this._createFormView();

		this._balloon = editor.plugins.get( ContextualBalloon );

		this._createToolbarColorButton();

		this._enableUserBalloonInteractions();
	}

	destroy()
	{
		super.destroy();

		this.formView.destroy();
	}

	updateInputs(color)
	{
		if (!color)
			return;

		if (this.useRGB)
		{
			//Set rgb values.
			this.formView.colorInputViewR.inputView.element.value = color['r'];
			this.formView.colorInputViewG.inputView.element.value = color['g'];
			this.formView.colorInputViewB.inputView.element.value = color['b'];
		}

		if (this.useHex)
		{
			this.formView.colorInputViewHex.inputView.element.value = color['hex'];
		}

		if (this.useOpacitySlider)
		{
			this.formView.opacitySlider.inputView.element.value = color['a'] * 10;
		}

		//Set current color to hex values.
		this.formView.currentColor = 'rgba(' + color['r'] + ', ' + color['g'] + ', ' + color['b'] + ', ' + color['a'] + ')';
		this.formView.colorIndicator.element.style.backgroundColor = this.formView.currentColor;
	}

	_createColorTemplateContainer(locale)
	{
		var colorTemplates = [];
		var colors = this.editor.config._config.customFontColors.colors;
		for (var i = 0; i < colors.length; i++)
		{
			//Safe check to ensure that the palette has working colors.
			if (isColor(colors[i]))
			{
				const colorTemplate = new ColorTemplate(locale, colors[i]);
				colorTemplates.push(colorTemplate);
			}
			else
			{
				//Notify the user that there is an invalid color in the config.
				console.warn('color: ' + '\'' + colors[i] + '\' is not a valid color. Please change it or check it.');
			}
		}
		var colorTemplateContainer = new ColorTemplateContainer(locale, colorTemplates);

		if (this.infoMessages)
		{
			console.info(
				'Loaded ' + 
				colors.length + 
				' template colors from config.');
		}

		return colorTemplateContainer;
	}

	updateRgba()
	{
		var opacity = this.useOpacitySlider ? this.formView.opacitySlider.inputView.element.value / 10 : 1;
		var t = 'rgba(' + 
		this.formView.colorInputViewR.inputView.element.value + ', ' +
		this.formView.colorInputViewG.inputView.element.value + ', ' +
		this.formView.colorInputViewB.inputView.element.value + ', ' +
		opacity + ') ';
		this.updateInputs(getColor(t));
	}

	_createFormView()
	{
		const editor = this.editor;
		const formView = new ColorUIView(editor.locale, this._createColorTemplateContainer(editor.locale), this.useRGB, this.useHex, this.useColorTemplates, this.useRemoveButton, this.useOpacitySlider);
		
		if (this.useHex)
		{
			//Update the RBG Input Fields if the Hex field changes.
			this.listenTo(formView.colorInputViewHex.inputView, 'input', () => {
				this.updateInputs(getColor(formView.colorInputViewHex.inputView.element.value));
			} );
		}
		
		if (this.useRGB)
		{
			//Update the Hex Input Field if any of the RGB fields change.
			this.listenTo(formView.colorInputViewR.inputView, 'input', () => {
				this.updateRgba();
			} );
			
			this.listenTo(formView.colorInputViewG.inputView, 'input', () => {
				this.updateRgba();
			} );
			
			this.listenTo(formView.colorInputViewB.inputView, 'input', () => {
				this.updateRgba();
			} );
		}
		
		if (this.useOpacitySlider)
		{
			this.listenTo(formView.opacitySlider.inputView, 'input', () => {
				this.updateRgba();
			} );
		}
		
		if (this.useRGB || this.useHex || this.useColorTemplates)
		{
			if (this.useRemoveButton)
			{
				//Listen to the remove button.
				this.listenTo(formView, 'removeColor', () => {
					this.formView.currentColor = null;
					formView.fire('changeColor');

					this._closeFormView();
				} );
			}

			//Actual change color function.
			this.listenTo(formView, 'changeColor', (color) => {
				//Last check to ensure that there are no errors. Null is valid because that removes the color.
				if (color.source.currentColor == null || isColor(color.source.currentColor))
				{
					editor.execute('fontColor', { value: color.source.currentColor });
				}
				else
					console.warn('The color you tried to set was not valid: ' + color.source.currentColor + '.');
			});
		}

		//Submit button.
		this.listenTo(formView, 'submit', () => {
			formView.fire('changeColor');
	
			this._closeFormView();
		} );

		//Listen to all colors templates.
		if (this.useColorTemplates)
		{
			for (var i = 0; i < formView.colorTemplateContainer.colorTemplates.length; i++)
			{
				var colorTemplate = formView.colorTemplateContainer.colorTemplates[i];
				this.listenTo(colorTemplate, 'execute', (colorTemplate) => {
					const color = getColor(colorTemplate.source.color);
					this.formView.currentColor = color['hex'];
					formView.fire('changeColor');
					//Update the inputfields.
					this.updateInputs(color);
				});
			}
		}

		this.listenTo(formView, 'cancel', () => {
			this.formView.currentColor = null;
			formView.fire('changeColor');
			this._closeFormView();
		} );
		
		formView.keystrokes.set('Esc', (data, cancel) => {
			this._closeFormView();
			cancel();
		} );

		return formView;
	}

	_createToolbarColorButton()
	{
		const editor = this.editor;
		const t = editor.t;

		editor.ui.componentFactory.add('customFontColors', locale => {
			const button = new ButtonView(locale);

			button.isEnabled = true;
			button.label = t('Font Color');
			button.icon = fontColorIcon;
			button.tooltip = true;

			this.listenTo(button, 'execute', () => this._showUI(true));

			return button;
		} );
	}

	_enableUserBalloonInteractions()
	{
		this.editor.keystrokes.set('Esc', (data, cancel) => {
			if (this._isUIVisible) {
				this._hideUI();
				cancel();
			}
		} );

		clickOutsideHandler( {
			emitter: this.formView,
			activator: () => this._isUIInPanel,
			contextElements: [this._balloon.view.element],
			callback: () => this._hideUI()
		} );
	}

	_addFormView()
	{
		if (this._isFormInPanel)
			return;

		this._balloon.add( {
			view: this.formView,
			position: this._getBalloonPositionData()
		} );

		if (this._balloon.visibleView === this.formView)
		{
			if (this.formView.colorInputViewHex)
				this.formView.colorInputViewHex.select();
			else if (this.formView.colorInputViewR)
				this.formView.colorInputViewR.select();
		}
	}
	
	_closeFormView()
	{
		const colorCommand = this.editor.commands.get('fontColor');

		if (colorCommand.value !== undefined)
			this._removeFormView();
		else
			this._hideUI();
	}

	_removeFormView()
	{
		if (this._isFormInPanel)
		{
			this.formView.saveButtonView.focus();

			this._balloon.remove(this.formView);

			this.editor.editing.view.focus();
		}
	}

	_showUI(forceVisible = false)
	{
		const editor = this.editor;
		const colorCommand = editor.commands.get('fontColor');

		if (!colorCommand.isEnabled)
			return;

		if (forceVisible)
			this._balloon.showStack('main');

		this._addFormView();
	}

	_hideUI()
	{
		if (!this._isUIInPanel)
			return;

		const editor = this.editor;

		editor.editing.view.focus();

		this._removeFormView();
	}

	get _isFormInPanel() 
	{
		return this._balloon.hasView(this.formView);
	}

	get _isUIInPanel()
	{
		return this._isFormInPanel;
	}

	get _isUIVisible()
	{
		const visibleView = this._balloon.visibleView;

		return visibleView == this.formView;
	}

	_getBalloonPositionData()
	{
		const view = this.editor.editing.view;
		const viewDocument = view.document;
		const target = view.domConverter.viewRangeToDom(viewDocument.selection.getFirstRange());

		return { target };
	}
}