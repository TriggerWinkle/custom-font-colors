import View from '@ckeditor/ckeditor5-ui/src/view';

export default class ColorIndicator extends View
{
	constructor(locale)
	{
		super(locale);

		this.setTemplate( {
			tag: 'div',
			attributes: {
				class: 'ck-colorindicator',
				style: 'width: 50px; height: 50px; margin: var(--ck-spacing-standard); border: 1px solid var(--ck-color-base-border);'
			}
		} );
	}
}