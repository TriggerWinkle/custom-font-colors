import ViewCollection from '@ckeditor/ckeditor5-ui/src/viewcollection';
import LabeledInputView from '@ckeditor/ckeditor5-ui/src/labeledinput/labeledinputview';
import InputTextView from '@ckeditor/ckeditor5-ui/src/inputtext/inputtextview';

import View from '@ckeditor/ckeditor5-ui/src/view';

import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import InputSliderView from '../ui/inputsliderview';
import submitHandler from '@ckeditor/ckeditor5-ui/src/bindings/submithandler';
import checkIcon from '@ckeditor/ckeditor5-core/theme/icons/check.svg';
import cancelIcon from '@ckeditor/ckeditor5-core/theme/icons/cancel.svg';

import ColorIndicator from '../ui/colorindicator';

import removeButtonIcon from '@ckeditor/ckeditor5-core/theme/icons/eraser.svg';

import { is_numeric } from '../utils';

export default class ColorUIView extends View
{
	//Create normal buttons (submit, cancel).
	_createButton( label, icon, className, eventName )
	{
		const button = new ButtonView(this.locale);

		button.set( {
			label,
			icon,
			tooltip: true
		} );

		button.extendTemplate( {
			attributes: {
				class: className
			}
		} );

		if (eventName)
			button.delegate('execute').to(this, eventName);

		return button;
	}

	//Create the color inputs.
	_createSlider(label, placeholder)
	{
		const t = this.locale.t;

		const labeledInput = new LabeledInputView(this.locale, InputSliderView);

		labeledInput.label = label;
		labeledInput.inputView.min = 0;
		labeledInput.inputView.max = 10;
		labeledInput.inputView.value = labeledInput.inputView.max;
		labeledInput.inputView.placeholder = placeholder;

		return labeledInput;
	}

	//Create the color inputs.
	_createColorInput(label, placeholder, onlyDigits = false)
	{
		const t = this.locale.t;

		const labeledInput = new LabeledInputView(this.locale, InputTextView);

		labeledInput.label = label;
		labeledInput.inputView.placeholder = placeholder;

		//If this input only accepts digits, add a listener and replace every non-numeric char.
		if (onlyDigits)
		{
			this.listenTo(labeledInput.inputView, 'input', (input) => {
				for (var i = 0; i < input.source.element.value.length; i++)
				{
					if (!is_numeric(input.source.element.value[i]))
					{
						input.source.element.value = input.source.element.value.replace(/[^\d]/g, '');
					}
				}
			} );
		}

		return labeledInput;
	}
	
	//Create the remove button.
	_createRemoveColorButton()
	{
		const buttonView = new ButtonView();

		buttonView.set( {
			withText: true,
			icon: removeButtonIcon,
			tooltip: true,
			label: 'Remove Color'
		} );

		buttonView.class = 'ck-color-table__remove-color';
		buttonView.delegate('execute').to(this, 'removeColor');

		return buttonView;
	}

	//Create the big color indicator.
	_createColorIndicator(locale)
	{
		const colorIndicatorView = new ColorIndicator(locale);

		return colorIndicatorView;
	}
	
	constructor(locale, colorTemplateContainer, useHex, useRGB, useColorTemplates, useRemoveButton, useOpacitySlider)
	{
		super(locale);

		const t = locale.t;
		this.useHex = useHex;
		this.useRGB = useRGB;
		this.useColorTemplates = useColorTemplates;
		this.useRemoveButton = useRemoveButton;
		this.useOpacitySlider = useOpacitySlider;
		
		if (this.useColorTemplates)
			this.colorTemplateContainer = colorTemplateContainer;
		if (this.useHex)
			this.colorInputViewHex = this._createColorInput('Hex Code', '#FEA49B');
		if (this.useRGB)
		{
			this.colorInputViewR = this._createColorInput('R', '255', true);
			this.colorInputViewG = this._createColorInput('G', '255', true);
			this.colorInputViewB = this._createColorInput('B', '255', true);
		}
		if (this.useRemoveButton)
			this.removeColorButton = this._createRemoveColorButton()

		if (this.useOpacitySlider)
			this.opacitySlider = this._createSlider('Opacity', '255');

		this.colorIndicator = this._createColorIndicator(locale);
		this.saveButtonView = this._createButton('Save', checkIcon, 'ck-button-save');
		this.saveButtonView.type = 'submit';
		this.cancelButtonView = this._createButton('Cancel', cancelIcon, 'ck-button-cancel', 'cancel');

		this.set('elementClass', 'bar');

		const childViews = [];
		
		//Add all the child elements like the inputs, color indicator and template colors.
		if (this.useColorTemplates)
			childViews.push(this.colorTemplateContainer);

		if (this.useColorTemplates || this.useHex || this.useRGB)
		{
			if (this.useRemoveButton)
				childViews.push(this.removeColorButton);
			childViews.push(this.colorIndicator);	
		}	
		
		if (this.useHex)
			childViews.push(this.colorInputViewHex);

		if (this.useRGB)
		{
			childViews.push(this.colorInputViewR);
			childViews.push(this.colorInputViewG);
			childViews.push(this.colorInputViewB);
		}

		if (this.useOpacitySlider)
			childViews.push(this.opacitySlider);

		childViews.push(this.saveButtonView);
		childViews.push(this.cancelButtonView);

		this.setTemplate( {
			tag: 'form',

			attributes: {
				class: [
					'ck',
					'ck-colorui-view',
				],
				style: 'padding: var(--ck-spacing-standard); width: 250px;',
				tabindex: '-1'
			},

			children: childViews
		} );
	}
	
	render()
	{
		super.render();

		submitHandler( {
			view: this
		} );

		const childViews = [];
		
		//Add all the child elements like the inputs, color indicator and template colors.
		if (this.useColorTemplates)
			childViews.push(this.colorTemplateContainer);

		if (this.useColorTemplates || this.useHex || this.useRGB)
		{
			if (this.useRemoveButton)
				childViews.push(this.removeColorButton);
			childViews.push(this.colorIndicator);	
		}	

		if (this.useHex)
			childViews.push(this.colorInputViewHex);

		if (this.useRGB)
		{
			childViews.push(this.colorInputViewR);
			childViews.push(this.colorInputViewG);
			childViews.push(this.colorInputViewB);
		}

		if (this.useOpacitySlider)
			childViews.push(this.opacitySlider);

		childViews.push(this.saveButtonView);
		childViews.push(this.cancelButtonView);

		this.keystrokes.listenTo(this.element);
	}
}