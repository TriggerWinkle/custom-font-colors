import View from '@ckeditor/ckeditor5-ui/src/view';

import '../../theme/fontcolor.css';

export default class ColorTemplateContainer extends View
{
	constructor(locale, colors)
	{
		super(locale);

		this.setTemplate( {
			tag: 'div',
			attributes: {
				class: 'ck-color-pallette-colorcontainer',
				style: 'display: inline-grid;\
                grid-template-columns: auto auto auto auto auto auto auto;\
                grid-gap: 3.5% 2.5%;\
                margin-bottom: 20px;\
                height: 100px;\
                overflow: hidden;\
                overflow-y: scroll;\
                width: 100%;\
                padding: 0px 80px 0 0;'
            },
            children: colors
        } );
        
        this.colorTemplates = colors;
	}
}