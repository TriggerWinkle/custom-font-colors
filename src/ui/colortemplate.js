import View from '@ckeditor/ckeditor5-ui/src/view';

import '../../theme/fontcolor.css';

export default class ColorTemplate extends View
{
	constructor(locale, color)
	{
		super(locale);

		const bind = this.bindTemplate;

		this.setTemplate( {
			tag: 'div',
			attributes: {
				class: 'ck-color-pallette-color',
				style: 'position: relative; width: 27.77px; height: 27.77px; border: 1px solid var(--ck-color-base-border); background-color: ' + color
			},
			on: {
                click: bind.to('execute')
            }
		} );

		this.color = color;
	}
}