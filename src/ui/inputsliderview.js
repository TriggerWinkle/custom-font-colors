import View from '@ckeditor/ckeditor5-ui/src/view';

export default class InputSliderView extends View
{
    constructor(locale)
    {
		super(locale);

		this.set('value');

		this.set('id');

		this.set('min');
		this.set('max');

		this.set('isReadOnly', false);

		this.set('hasError', false);

		this.set('ariaDescribedById');

		const bind = this.bindTemplate;

		this.setTemplate( {
			tag: 'input',
			attributes: {
				type: 'range',
				min: bind.to('min'),
				max: bind.to('max'),
				class: [
					'ck',
					'ck-input',
					'ck-input-text',
					bind.if( 'hasError', 'ck-error' )
				],
				style: "box-shadow: none;",
				id: bind.to('id'),
				readonly: bind.to( 'isReadOnly' ),
				'aria-invalid': bind.if( 'hasError', true ),
				'aria-describedby': bind.to( 'ariaDescribedById' )
			},
			on: {
				input: bind.to('input')
			}
		} );
	}

	render()
	{
		super.render();

		const setValue = value => {
			this.element.value = (!value && value !== 0) ? '' : value;
		};

		const setMin = value => {
			this.element.min = (!value && value !== 0) ? '' : value;
		};

		const setMax = value => {
			this.element.max = (!value && value !== 0) ? '' : value;
		};

		setValue(this.value);
		setMin(this.min);
		setMax(this.max);

		this.on('change:value', (evt, name, value) => {
			setValue(value);
		} );
	}

	select()
	{
		this.element.select();
	}

	focus()
	{
		this.element.focus();
	}
}
