export function renderUpcastAttribute(styleAttr)
{
	return viewElement => normalizeColorCode(viewElement.getStyle( styleAttr ));
}

export function renderDowncastElement(styleAttr)
{
	//Create the color attribute.
	return (modelAttributeValue, viewWriter) => viewWriter.createAttributeElement('span', {
		style: styleAttr + ':' + modelAttributeValue
	} );
}

function normalizeColorCode( value )
{
	return value.replace( /\s/g, '' );
}

//Simple function to check whether a string is empty or only contains whitespace.
export function isStringEmpty(string)
{
	if (!string || string  == undefined)
		return true;

	var result = string.toString().replace(/ /g, '');
	return (result === '');
}

//For converting RGB to hex
export function RGBToHex(rgb)
{
	rgb = rgb.match(/rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?, ([0-9.,]*[0-9])/i);

	return rgb ? '#' +
		('0' + parseInt(rgb[1],10).toString(16)).slice(-2) +
		('0' + parseInt(rgb[2],10).toString(16)).slice(-2) +
		('0' + parseInt(rgb[3],10).toString(16)).slice(-2) :
	null;
}

//For converting hex to rgb.
export function hexToRGB(hex)
{
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	if (!result)
		return;
	var r = parseInt(result[1], 16);
	var g = parseInt(result[2], 16);
	var b = parseInt(result[3], 16);
	var a = 1;
	return result ? {
		r: r,
		g: g,
		b: b,
		a: a
	} : null
}

export function getHexColor(hex)
{
	//If the hex is valid, no need to convert.
	return isHexColor(hex) ? {
		hex: hex
	} : null
}

export function getColorByHex(hex)
{
	//Get hex color.
	var color = getHexColor(hex);

	//Set rgb values.
	if (color)
	{
		color['r'] = hexToRGB(color['hex'])['r'];
		color['g'] = hexToRGB(color['hex'])['g'];
		color['b'] = hexToRGB(color['hex'])['b'];
		color['a'] = 1;
	}

	return color;
}

export function getColorByRgba(rgba)
{
	rgba = rgba.match(/rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?, ([0-9.,]*[0-9])\)/i);

	//Set rgb values.
	var color = {};
	color['r'] = parseInt(rgba[1]);
	color['g'] = parseInt(rgba[2]);
	color['b'] = parseInt(rgba[3]);
	color['a'] = parseFloat(rgba[4]);
	color['hex'] = RGBToHex(rgba[0]);

	return color;
}

export function getColor(value)
{
	//Get the color by either hex or rgb. Else it will return null.
	if (isHexColor(value))
		return getColorByHex(value);

	if (isRgbColor(value))
		return getColorByRgba(value);
	
	return null;
}

export function isHexColor(value)
{
	var result = /^#([0-9a-f]{6}|[0-9a-f]{3})$/i.test(value);

	//Return the result of the regex, but check if the length is 6.
	return result && (value.charAt(0) == '#' && value.substr(1).length == 6 || value.length == 6);
}

export function isRgbColor(value)
{
	//Get everything including the rgb(a) part.
	var result = /rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?, ([0-9.,]*[0-9])/i.test(value);
	return result;
}

//Just a general function that checks if the color is even a color.
export function isColor(value)
{
	return isRgbColor(value) || isHexColor(value);
}

export function is_numeric(str)
{
	return /^\d+$/.test(str);
}