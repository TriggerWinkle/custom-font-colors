CKEditor 5 font feature
============================

made by: Kylian Dekker

This package implements support for the following CKEditor 5 features:

* font color,

The original plugin was made by CKEditor-5.

## Installation

Place the plugin in your plugins folder and import it in your app.js.

```
import CustomFontColors from 'ckeditor5-customfontcolors/src/fontcolor/fontcolor';
```

Include it in your plugins:

```
ClassicEditor
    .create( document.querySelector( '#editor' ), {
        // Add Image to the plugin list.
        plugins: [ CustomFontColors ],

        // ...
    } )
    // ..
```

And include it in the toolbar:

```
toolbar: {
	items: [
		'customFontColors'
	]
}
```

## Usage

I have added a few options for customization.

You can enable the RGB features or disable them completely.
```
RGB: true
```

You can enable the Hex features or disable them completely.
```
hex: true
```

You can enable the color template features or disable them completely.
```
colorTemplates: true
```

You should then define your own color templates.
```
colors: [
	'rgba(255, 0, 0, 1)',
	'#fea49b',
	'#00FF90',
	'#57007F',
	'#00FFFF',
	'#FFE97F', 
	'#303030',
	'#A5FF7F', 
	'rgba(255, 0, 255, 1)',
	'#FFE77F', 
	'#0094FF',
	'#FF6A00', 
	'rgba(0, 255, 40, 1)',
	'#7F3300', 
	'#A17FFF',
	'#7F6A00', 
	'#303030',
	'#A5FF7F', 
	'#D67FFF',
	'#FFE77F', 
	'#0094FF'
]
```

You can enable the remove color button feature or disable it completely.
```
removeButton: true
```

You can enable the opacity slider feature or disable it completely.
```
opacitySlider: true
```

You can enable or disable debug/info messages from the plugin.
```
infoMessages: true
```

## Contributing
If you have any improvements or want any features let me know.